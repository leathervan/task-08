package com.epam.rd.java.basic.task8.constants;

public class Constants {
    private Constants() {
    }

    public static final String VALID_XML_FILE = "input.xml";
    public static final String INVALID_XML_FILE = "invalidXML.xml";
    public static final String XSD_FILE = "input.xsd";

    public static final String FEATURE_TURN_VALIDATION_ON
            = "http://xml.org/sax/features/validation";
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON
            = "http://apache.org/xml/features/validation/schema";

    public static final String FEATURE_ONE
            = "http://xml.org/sax/features/external-parameter-entities";
    public static final String FEATURE_TWO
            = "http://apache.org/xml/features/disallow-doctype-decl";
    public static final String FEATURE_THREE
            = "http://xml.org/sax/features/external-general-entities";
}
