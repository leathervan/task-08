package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.util.Sorter;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1){
			return;
		}
		String xmlFileName = args[0];

		//DOM
		DOMController domCtrl = new DOMController(xmlFileName);
		domCtrl.parse(true);

		Flowers flowers = domCtrl.getFlowers();
		Sorter.sortFlowerName(flowers);

		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(flowers,outputXmlFile);

		//SAX
		SAXController saxCtrl = new SAXController(xmlFileName);
		saxCtrl.parse(true);

		flowers = saxCtrl.getFlowers();
		Sorter.sortFlowerSoil(flowers);

		outputXmlFile = "output.sax.xml";
		DOMController.saveToXML(flowers,outputXmlFile);

		//STAX
		STAXController staxCtrl = new STAXController(xmlFileName);
		staxCtrl.parse();

		flowers = staxCtrl.getFlowers();
		Sorter.sortFlowerOrigin(flowers);

		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(flowers,outputXmlFile);
	}

}
