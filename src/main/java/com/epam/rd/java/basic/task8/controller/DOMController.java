package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.growing.GrowingTips;
import com.epam.rd.java.basic.task8.entity.growing.Lighting;
import com.epam.rd.java.basic.task8.entity.growing.Tempreture;
import com.epam.rd.java.basic.task8.entity.growing.Watering;
import com.epam.rd.java.basic.task8.entity.parameters.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.parameters.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private final String xmlFileName;
	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers() {
		return flowers;
	}

	public static Document getDocument(Flowers flowers)
			throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature(Constants.FEATURE_ONE, false);
		dbf.setFeature(Constants.FEATURE_TWO, true);
		dbf.setFeature(Constants.FEATURE_THREE, false);
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		Element flowersElement = document.createElement(XML.FLOWERS.value());
		document.appendChild(flowersElement);

		for (Flower flower : flowers.getFlower()) {
			extractedFlower(document, flowersElement, flower);
		}
		return document;
	}

	private static void extractedFlower(Document document, Element flowersElement, Flower flower) {
		Element flowerElement = document.createElement(XML.FLOWER.value());
		flowersElement.appendChild(flowerElement);

		Element nameElement = document.createElement(XML.FLOWER_NAME.value());
		nameElement.setTextContent(flower.getName());
		flowerElement.appendChild(nameElement);

		Element soilElement = document.createElement(XML.FLOWER_SOIL.value());
		soilElement.setTextContent(flower.getSoil());
		flowerElement.appendChild(soilElement);

		Element originElement = document.createElement(XML.FLOWER_ORIGIN.value());
		originElement.setTextContent(flower.getOrigin());
		flowerElement.appendChild(originElement);

		extractedVisualParameters(document, flower, flowerElement);
		extractedGrowingTips(document, flower, flowerElement);

		Element multiplyingElement = document.createElement(XML.FLOWER_MULTIPLYING.value());
		multiplyingElement.setTextContent(flower.getMultiplying());
		flowerElement.appendChild(multiplyingElement);
	}

	private static void extractedVisualParameters(Document document, Flower flower, Element flowerElement) {
		for (VisualParameters vp : flower.getVisualParameters()) {
			Element vpElement = document.createElement(XML.VISUAL_PARAMETERS.value());

			Element scElement = document.createElement(XML.STEM_COLOUR.value());
			scElement.setTextContent(vp.getStemColour());
			vpElement.appendChild(scElement);

			Element lcElement = document.createElement(XML.LEAF_COLOUR.value());
			lcElement.setTextContent(vp.getLeafColour());
			vpElement.appendChild(lcElement);

			for (AveLenFlower alf : vp.getAveLenFlower()) {
				Element alfElement = document.createElement(XML.AVE_LEN_FLOWER.value());
				alfElement.setTextContent(String.valueOf(alf.getContent()));
				alfElement.setAttribute(XML.MEASURE.value(), alf.getMeasure());
				vpElement.appendChild(alfElement);
			}
			flowerElement.appendChild(vpElement);
		}
	}

	private static void extractedGrowingTips(Document document, Flower flower, Element flowerElement) {
		for (GrowingTips gt : flower.getGrowingTips()) {
			Element gtElement = document.createElement(XML.GROWING_TIPS.value());

			for (Tempreture t : gt.getTemperature()) {
				Element tElement = document.createElement(XML.TEMPRETURE.value());
				tElement.setTextContent(String.valueOf(t.getContent()));
				tElement.setAttribute(XML.MEASURE.value(), t.getMeasure());
				gtElement.appendChild(tElement);
			}
			for (Lighting l : gt.getLighting()) {
				Element lElement = document.createElement(XML.LIGHTING.value());
				lElement.setAttribute(XML.LIGHT_REQUIRING.value(), l.getLightRequiring());
				gtElement.appendChild(lElement);
			}
			for (Watering w : gt.getWatering()) {
				Element wElement = document.createElement(XML.WATERING.value());
				wElement.setTextContent(String.valueOf(w.getContent()));
				wElement.setAttribute(XML.MEASURE.value(), w.getMeasure());
				gtElement.appendChild(wElement);
			}
			flowerElement.appendChild(gtElement);
		}
	}

	public void parse(boolean validate)
			throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature(Constants.FEATURE_ONE, false);
		dbf.setFeature(Constants.FEATURE_TWO, true);
		dbf.setFeature(Constants.FEATURE_THREE, false);
		dbf.setNamespaceAware(true);

		if (validate) {
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e;
			}
		});
		Document document = db.parse(xmlFileName);
		flowers = new Flowers();
		NodeList flowerNode = document.getElementsByTagName(XML.FLOWER.value());
		for (int i = 0; i < flowerNode.getLength(); ++i) {
			Flower flower = getFlower(flowerNode.item(i));
			flowers.getFlower().add(flower);
		}
	}

	private Flower getFlower(Node flowerNode) {
		Flower flower = new Flower();
		Element element = (Element) flowerNode;
		Node node;

		node = element.getElementsByTagName(XML.FLOWER_NAME.value()).item(0);
		flower.setName(node.getTextContent());

		node = element.getElementsByTagName(XML.FLOWER_SOIL.value()).item(0);
		flower.setSoil(node.getTextContent());

		node = element.getElementsByTagName(XML.FLOWER_ORIGIN.value()).item(0);
		flower.setOrigin(node.getTextContent());

		NodeList vpNodeList = element.getElementsByTagName(XML.VISUAL_PARAMETERS.value());
		for (int i = 0; i < vpNodeList.getLength(); ++i) {
			VisualParameters vp = getVisualParameters(vpNodeList.item(i));
			flower.getVisualParameters().add(vp);
		}

		NodeList gtNodeList = element.getElementsByTagName(XML.GROWING_TIPS.value());
		for (int i = 0; i < gtNodeList.getLength(); ++i) {
			GrowingTips gt = getGrowingTips(gtNodeList.item(i));
			flower.getGrowingTips().add(gt);
		}

		node = element.getElementsByTagName(XML.FLOWER_MULTIPLYING.value()).item(0);
		flower.setMultiplying(node.getTextContent());

		return flower;
	}

	private VisualParameters getVisualParameters(Node vpNode) {
		VisualParameters vp = new VisualParameters();
		Element element = (Element) vpNode;
		Node node;

		node = element.getElementsByTagName(XML.STEM_COLOUR.value()).item(0);
		vp.setStemColour(node.getTextContent());

		node = element.getElementsByTagName(XML.LEAF_COLOUR.value()).item(0);
		vp.setLeafColour(node.getTextContent());

		NodeList alfNodeList = element.getElementsByTagName(XML.AVE_LEN_FLOWER.value());
		for (int i = 0; i < alfNodeList.getLength(); ++i) {
			AveLenFlower alf = getAveLenFlower(alfNodeList.item(i));
			vp.getAveLenFlower().add(alf);
		}
		return vp;
	}

	private AveLenFlower getAveLenFlower(Node alfNode) {
		AveLenFlower alf = new AveLenFlower();
		Element element = (Element) alfNode;

		String content = element.getTextContent();
		alf.setContent(Integer.parseInt(content));

		String measure = element.getAttribute(XML.MEASURE.value());
		alf.setMeasure(measure);

		return alf;
	}

	private GrowingTips getGrowingTips(Node gtNode) {
		GrowingTips gt = new GrowingTips();
		Element element = (Element) gtNode;

		NodeList temperatureNodeList = element.getElementsByTagName(XML.TEMPRETURE.value());
		for (int i = 0; i < temperatureNodeList.getLength(); ++i) {
			Tempreture tempreture = getTemperature(temperatureNodeList.item(i));
			gt.getTemperature().add(tempreture);
		}

		NodeList lightingNodeList = element.getElementsByTagName(XML.LIGHTING.value());
		for (int i = 0; i < lightingNodeList.getLength(); ++i) {
			Lighting lighting = getLighting(lightingNodeList.item(i));
			gt.getLighting().add(lighting);
		}

		NodeList wateringNodeList = element.getElementsByTagName(XML.WATERING.value());
		for (int i = 0; i < wateringNodeList.getLength(); ++i) {
			Watering watering = getWatering(wateringNodeList.item(i));
			gt.getWatering().add(watering);
		}

		return gt;
	}

	private Tempreture getTemperature(Node tNode) {
		Tempreture tempreture = new Tempreture();
		Element element = (Element) tNode;

		String content = element.getTextContent();
		tempreture.setContent(Integer.parseInt(content));

		String measure = element.getAttribute(XML.MEASURE.value());
		tempreture.setMeasure(measure);

		return tempreture;
	}

	private Lighting getLighting(Node lNode) {
		Lighting lighting = new Lighting();
		Element element = (Element) lNode;

		String lightRequiring = element.getAttribute(XML.LIGHT_REQUIRING.value());
		lighting.setLightRequiring(lightRequiring);

		return lighting;
	}

	private Watering getWatering(Node wNode) {
		Watering watering = new Watering();
		Element element = (Element) wNode;

		String content = element.getTextContent();
		watering.setContent(Integer.parseInt(content));

		String measure = element.getAttribute(XML.MEASURE.value());
		watering.setMeasure(measure);

		return watering;
	}

	public static void saveToXML(Flowers flowers, String outputXmlFile)
			throws ParserConfigurationException, TransformerException {
		saveToXML(getDocument(flowers), outputXmlFile);
	}

	public static void saveToXML(Document document, String outputXmlFile)
			throws TransformerException {
		StreamResult streamResult = new StreamResult(new File(outputXmlFile));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(new DOMSource(document), streamResult);
	}

}
