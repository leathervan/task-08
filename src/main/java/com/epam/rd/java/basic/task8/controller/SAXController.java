package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.growing.GrowingTips;
import com.epam.rd.java.basic.task8.entity.growing.Lighting;
import com.epam.rd.java.basic.task8.entity.growing.Tempreture;
import com.epam.rd.java.basic.task8.entity.growing.Watering;
import com.epam.rd.java.basic.task8.entity.parameters.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.parameters.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	private final String xmlFileName;
	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	private AveLenFlower aveLenFlower;
	private Tempreture tempreture;
	private Lighting lighting;
	private Watering watering;
	private String currentElement;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public void parse(boolean validate)
			throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		saxParserFactory.setFeature(Constants.FEATURE_ONE, false);
		saxParserFactory.setFeature(Constants.FEATURE_TWO, true);
		saxParserFactory.setFeature(Constants.FEATURE_THREE, false);
		saxParserFactory.setNamespaceAware(true);
		if (validate) {
			saxParserFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			saxParserFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}
		SAXParser saxParser = saxParserFactory.newSAXParser();
		saxParser.parse(xmlFileName, this);
	}

	@Override
	public void error(SAXParseException e) throws SAXException {
		throw e;
	}

	public Flowers getFlowers() {
		return flowers;
	}

	@Override
	public void startElement(String uri, String localName,
							 String qName, Attributes attributes) {
		uri = "";
		currentElement = localName;
		if (XML.FLOWERS.equalsTo(currentElement)) {
			flowers = new Flowers();
			return;
		}
		if (XML.FLOWER.value().equals(currentElement)) {
			flower = new Flower();
			return;
		}
		if (XML.VISUAL_PARAMETERS.equalsTo(currentElement)) {
			visualParameters = new VisualParameters();
			return;
		}
		if (XML.GROWING_TIPS.equalsTo(currentElement)) {
			growingTips = new GrowingTips();
			return;
		}
		if (XML.AVE_LEN_FLOWER.equalsTo(currentElement)) {
			extractedALF(uri, attributes);
			return;
		}
		if (XML.TEMPRETURE.equalsTo(currentElement)) {
			extractedTemperature(uri, attributes);
			return;
		}
		if (XML.LIGHTING.equalsTo(currentElement)) {
			extractedLightning(uri, attributes);
			return;
		}
		if (XML.WATERING.equalsTo(currentElement)) {
			extractedWatering(uri, attributes);
		}
	}

	private void extractedALF(String uri, Attributes attributes) {
		aveLenFlower = new AveLenFlower();
		if (attributes.getLength() > 0) {
			aveLenFlower.setMeasure(attributes
					.getValue(uri, XML.MEASURE.value()));
		}
	}

	private void extractedTemperature(String uri, Attributes attributes) {
		tempreture = new Tempreture();
		if (attributes.getLength() > 0) {
			tempreture.setMeasure(attributes
					.getValue(uri, XML.MEASURE.value()));
		}
	}

	private void extractedLightning(String uri, Attributes attributes) {
		lighting = new Lighting();
		if (attributes.getLength() > 0) {
			lighting.setLightRequiring(attributes
					.getValue(uri, XML.LIGHT_REQUIRING.value()));
		}
	}

	private void extractedWatering(String uri, Attributes attributes) {
		watering = new Watering();
		if (attributes.getLength() > 0) {
			watering.setMeasure(attributes
					.getValue(uri, XML.MEASURE.value()));
		}
	}



	@Override
	public void characters(char[] ch, int start, int length) {
		String elementText = new String(ch, start, length).trim();
		if (elementText.isEmpty()) {
			return;
		}
		if (XML.FLOWER_NAME.equalsTo(currentElement)) {
			flower.setName(elementText);
			return;
		}
		if (XML.FLOWER_SOIL.equalsTo(currentElement)) {
			flower.setSoil(elementText);
			return;
		}
		if (XML.FLOWER_ORIGIN.equalsTo(currentElement)) {
			flower.setOrigin(elementText);
			return;
		}
		if (XML.STEM_COLOUR.equalsTo(currentElement)) {
			visualParameters.setStemColour(elementText);
			return;
		}
		if (XML.LEAF_COLOUR.equalsTo(currentElement)) {
			visualParameters.setLeafColour(elementText);
			return;
		}
		if (XML.AVE_LEN_FLOWER.equalsTo(currentElement)) {
			aveLenFlower.setContent(Integer.parseInt(elementText));
			return;
		}
		if (XML.TEMPRETURE.equalsTo(currentElement)) {
			tempreture.setContent(Integer.parseInt(elementText));
			return;
		}
		if (XML.WATERING.equalsTo(currentElement)) {
			watering.setContent(Integer.parseInt(elementText));
			return;
		}
		if (XML.FLOWER_MULTIPLYING.equalsTo(currentElement)) {
			flower.setMultiplying(elementText);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (XML.FLOWER.equalsTo(localName)) {
			flowers.getFlower().add(flower);
			return;
		}
		if (XML.VISUAL_PARAMETERS.equalsTo(localName)) {
			flower.getVisualParameters().add(visualParameters);
			return;
		}
		if (XML.GROWING_TIPS.equalsTo(localName)) {
			flower.getGrowingTips().add(growingTips);
			return;
		}
		if (XML.AVE_LEN_FLOWER.equalsTo(localName)) {
			visualParameters.getAveLenFlower().add(aveLenFlower);
			return;
		}
		if (XML.TEMPRETURE.equalsTo(localName)) {
			growingTips.getTemperature().add(tempreture);
			return;
		}
		if (XML.LIGHTING.equalsTo(localName)) {
			growingTips.getLighting().add(lighting);
			return;
		}
		if (XML.WATERING.equalsTo(localName)) {
			growingTips.getWatering().add(watering);
		}
	}

}